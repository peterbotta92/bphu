let gulp = require('gulp')
let sass = require('gulp-sass')
let pug = require('gulp-pug')
let server = require('browser-sync').create()

var bsError = function(data) {
	browserSync.sockets.emit('fullscreen:message', {
		title: data.plugin + ': Lorem Ipsum ' +data.name ,
		body: data.message
	}
)};

function reload(done) {
  server.reload()
  done()
}

function serve(done) {
  server.init({
    server: {
      baseDir: './build',
      routes: {
        '/public' : 'public',
        '/images': 'public/images'
      }
    },
    plugins: ['bs-pretty-message']
  })
  done()
}

// Style
function style(cb) {
  return (
    gulp.src('src/styles/**/*.scss')
      .pipe(sass())
      .on('error', sass.logError)
      .pipe(gulp.dest('build/styles'))
      .on('error', e => {
        log.error(colors.red(`[ERROR] CSS build was failed: ${e}`));
      })
  )
}

// Template
function template(cb) {
  return (
    gulp.src('src/templates/*.pug')
      .pipe(pug({
        pretty: true
      }))
      .pipe(gulp.dest('build'))
  )
}

// Javascript
function javascript(cb) {
	return (
    gulp.src('src/js/*.js')
    	.pipe(gulp.dest('build/js'))
  )
}


function watch() {
  gulp.watch('src/styles/**/*.scss', gulp.series(style, reload))
  gulp.watch('src/templates/**/*.pug', gulp.series(template, reload))
	gulp.watch('src/js/**/*.js', gulp.series(javascript, reload))
}

exports.style = style
exports.template = template
exports.serve = serve
exports.dev = gulp.series(style, template, serve, watch)
