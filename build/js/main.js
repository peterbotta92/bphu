$( document ).ready(function() {

  $(window).scroll(function() {
    var scroll = $(window).scrollTop();
    if (scroll >= 5) {
        $('header').addClass('scrolled');
    } else {
        $('header').removeClass('scrolled');
    }
  });

  $('#menuToggle').on('click', function(){
    $('#menu').toggleClass('opened');
  });

  $('.landing').on('click', function(){
    if ($('#menu').hasClass('opened')) {
      $('#menuToggle input').trigger('click');
    }
  });

  $('#menu .close').on('click', function(){
    $('#menuToggle input').trigger('click');
  });

  $('.login').on('click', function(){
    $('header').addClass('logged-in');
  });

  $('.logout').on('click', function(){
    $('header').removeClass('logged-in');
    $('.user-actions').hide();
  });

  $('.actions .avatar').on('mouseenter', function(){
    $('ul.user-actions').css('display', 'block');
  });

  $('ul.user-actions').on('mouseenter', function(){
    $('ul.user-actions').css('display', 'block');
  });

  setTimeout(function() {
    $('ul.user-actions').on('mouseleave', function(){
      $('ul.user-actions').css('display', 'none');
    });
  }, 1000);

  $('.actions .avatar').on('mouseleave', function(){
    $('ul.user-actions').css('display', 'none');
  });
});
